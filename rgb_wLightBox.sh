#!/bin/bash
#by Jakub Krol 17.06.2019r.

#long x, long in_min, long in_max, long out_min, long out_max
map() {
  map_return=$(($(($((${1}-${2})) * $((${5}-${4})))) / $((${3}-${2}))+${4}))
}

temp_to_color() {
	red=${1}
}

usage() {
	case "${1}" in	
		0)
			echo ""
			echo "- - - - - - - - - - - - - - - - - - - - - - - "
			echo "Usage: ${0}"
			echo "- - - - - - - - - - - - - - - - - - - - - - - "
			echo ""
			;;
		i)
			echo "-i	ip of wLightBox: -i <xxx.xxx.xxx.xxx>"
			;;
		a)
			echo "-a	expected ambient temperature (blue color max): -a <10 +99>"
			;;
		l)
			echo "-l	maximum expected load temperature (red color max): -l <10 +99>"
			;;
		t)
			echo "-t	http request - time interval in seconds (sleep): -t <1 720>"
			;;
		c)
			echo "-c	command to check temperature: -c <string>"
			;;
		g)
			echo "-g	phrase to grep: -g <phrase>"
			;;
		connection)
			echo "reason: no connection or bad ip"
			echo ""
			;;
		device)
			echo "reason: choose correct BleBox device"
			echo ""
			;;
			
		al) 
			echo "reason: minimum temperature > maximum temperature"
			echo ""
			;;
		-1) 
			echo ""
			echo "- - - - - - - - - - - - - - - - - - - - - - - "
			echo "  Well, try fill this in correct way: "
			echo "- - - - - - - - - - - - - - - - - - - - - - - "
			echo ""
			;;
		empty)
			;;
	esac


	if [ $2 == 1 ]; then
		echo ""
		echo "- - - - - - - - - - - - - - - - - - - - - - - "
		echo ""
		exit 1;	
	fi
}

while getopts "i:a:l:t:c:g:h" arg; do
	case "${arg}" in
		i)
			box_ip=${OPTARG}
			;;
		a)
			min_temp=${OPTARG}
			if [ "${min_temp}" -lt $"10" ] || [ "${min_temp}" -gt $"99" ]; then
				usage -1 0
				usage a 1
			fi
			;;
		l)
			max_temp=${OPTARG}
			if [ "${max_temp}" -lt $"10" ] || [ "${max_temp}" -gt $"99" ]; then
				usage -1 0
				usage l 1
			fi
			;;
		t)
			time_interval=${OPTARG}
			if [ "${min_temp}" -lt $"1" ] || [ "${min_temp}" -gt $"720" ]; then
				usage -1 0
				usage t 1
			fi
			;;
		c)
			command=${OPTARG}
			;;
		g)
			grep_text=${OPTARG}
			;;
		*)
			usage 0 0
			usage i 0
			usage a 0
			usage l 0
			usage t 0
			usage c 0
			usage g 1
			;;
		h)	
			usage 0 0
			usage i 0
			usage a 0
			usage l 0
			usage t 0
			usage c 0
			usage g 1
			;;
	esac
done
shift $((OPTIND-1))


# if empty
if [ -z "${box_ip}" ] || [ -z "${min_temp}" ] || [ -z "${max_temp}" ] || [ -z "${time_interval}" ] || [ -z "${command}" ] || [ -z "${grep_text}" ]; then
	usage 0 0
	usage i 0
	usage a 0
	usage l 0
	usage t 0
	usage c 0
	usage g 1
fi

if [ "${max_temp}" -le "${min_temp}" ]; then
	usage -1 0
	usage al 0
	usage a 0
	usage l 1
fi

response=$(curl -s ${box_ip}"/api/device/state")
checkDevice_flag=$(echo ${response} | grep "wLightBox")

if [ "${response}" == "" ]; then
	usage -1 0
	usage connection 0
	usage i 1
fi

if [ "${checkDevice_flag}" == "" ]; then
	usage -1 0
	usage device 0
	usage i 1
fi

echo ""
echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
echo "    	wLightBox - temperature to color script	       "
echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
echo ""
echo "IP of wLightBox controller 		= ${box_ip}"
echo "minimum temperature (blue color)	= ${min_temp} [C]"
echo "maximum temperature (red color)		= ${max_temp} [C]"
echo "time interval (http request) 		= ${time_interval} [s]"
echo "command to check temperatures 		= ${command}"
echo "phrase to grep temperature 		= ${grep_text}"
echo ""
echo "Starting in background ..."
echo ""
echo "- - - - - - - - - - - - - - - - - - - - - - - - - - - - -"
echo ""

set_state=${box_ip}"/s/"

while /bin/true; do
	temp=$(${command} | grep ${grep_text} | grep -Eo [0-9][0-9])	

	#long x, long in_min, long in_max, long out_min, long out_max
	map ${temp} ${min_temp} ${max_temp} $"0" $"255"

	color_threshold=$(($(($((${max_temp}-${min_temp}))/2))+${min_temp}))

	red=$"0"
	blue=$"0"
	green=$"0"
	white=$"0"

	if [ "${temp}" -ge "${max_temp}" ]; then 
		temp=${max_temp}
		red=$"255"
		blue=$"0"
		green=$"0"
		white=$"0"
	else 

		if [ "${temp}" -le "${min_temp}" ]; then 
			temp=${min_temp}
			red=$"0"
			blue=$"255"
			green=$"0"
			white=$"0"	
		else

			if [ "${temp}" -ge "${color_threshold}" ]; then
				red=$"255"
				map ${temp} ${color_threshold} ${max_temp} $"0" $"127"
				green=$(($"127"-${map_return}))
				blue=$"0"
			else
				map ${temp} ${min_temp} ${color_threshold} $"0" $"255"
				red=${map_return}
				map ${temp} ${min_temp} ${color_threshold} $"0" $"127"
				green=${map_return}
				map ${temp} ${min_temp} ${color_threshold} $"0" $"255"
				blue=$(($"255"-${map_return}))
			fi
		
		fi
	fi

	red=$(printf "%02x" $red)
	blue=$(printf "%02x" $blue)
	green=$(printf "%02x" $green)
	white=$(printf "%02x" $white)

	response=$(curl -s ${set_state}${red}${green}${blue}${white})

	sleep ${time_interval}
done &
