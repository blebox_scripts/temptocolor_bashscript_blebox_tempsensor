#### Example of use
For raspberry pi it is something like this: 

```sh
./rgb_wLightBox.sh -i 192.168.9.194 -a 35 -l 60 -t 1 -c "vcgencmd measure_temp" -g temp
```

```
- - - - - - - - - - - - - - - - - - - - - - - 
Usage: ./rgb_wLightBox.sh
- - - - - - - - - - - - - - - - - - - - - - - 

-i	ip of wLightBox: -i <xxx.xxx.xxx.xxx>
-a	expected ambient temperature (blue color max): -a <10 +99>
-l	maximum expected load temperature (red color max): -l <10 +99>
-t	http request - time interval in seconds (sleep): -t <1 720>
-c	command to check temperature: -c <string>
-g	phrase to grep: -g <phrase>

- - - - - - - - - - - - - - - - - - - - - - - 
```
